#!/bin/sh

BASE_DIR="$( cd $(dirname "$0") ; pwd)"
cd "${BASE_DIR}"

set -eux

# Build and push runner image
DOCKER_IMAGE="registry.gitlab.com/phmgmt-public/containers/shell:latest"
docker build -t "${DOCKER_IMAGE}" shell
docker push "${DOCKER_IMAGE}"
