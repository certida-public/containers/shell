. /root/.bashrc_kube_ps1

kube_shell() {
  if [ -z "$1" ]; then
    echo "No namespace specified."
    return
  fi
  pod_ns="$1"

  if [ ! -z "$2" ]; then
    pod_exec="/bin/sh"
    pod_find="$2"
  else
    pod_exec="/bin/bash"
    pod_find="(0|ph-)shell"
  fi

  pod_name=$(kubectl -n "${pod_ns}" get pod | egrep "^${pod_find}" | awk '{print $1}' | head -1)

  if [ -z "${pod_name}" ]; then
    echo "No '${pod_find}' pod running in namespace '${pod_ns}'."
  else
    kubectl -n "${pod_ns}" exec -it "${pod_name}" "${pod_exec}"
  fi
}


PS1='\e[0m\n[\[\e[01;36m\]\u@\h\[\e[00m\] \[\e[01;34m\]\w\[\e[00m\]]${debian_chroot:+ \[\e[1;33m\]{$debian_chroot\}}${VIRTUAL_ENV:+ \[\e[0;36m\]{$VIRTUAL_ENV\}}\[\e[0m\]$(kube_ps1)\n\$ '

export EDITOR=vim
export LESS=-eMQRw
export DOTNET_CLI_TELEMETRY_OPTOUT=1
export RABBITMQ_NODENAME='rabbit@rabbitmq'
export VIRTUAL_ENV_DISABLE_PROMPT=1

alias vi=vim
alias amqp-watch=amqp-watch.sh
alias aws-ls-ec2="aws ec2 describe-instances --output table --query \"Reservations[].Instances[].[InstanceId,InstanceType,PrivateIpAddress,PublicIpAddress,State.Name,Tags[?Key==\\\`Name\\\`].Value[]|[0]] | sort_by(@, &join('', [to_string([4]), to_string([5]), to_string([1])]))\""
alias aws-asg-scalein="aws autoscaling terminate-instance-in-auto-scaling-group --output table --should-decrement-desired-capacity --instance-id"
alias kube-shell='kube_shell'

if [ -f "/root/.bashrc_local" ]; then
  . /root/.bashrc_local
fi
