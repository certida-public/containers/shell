set sw=4
set expandtab

hi Comment cterm=BOLD ctermfg=0
hi DiffAdd ctermbg=22
hi DiffChange ctermbg=53
hi DiffDelete ctermbg=52

hi DiffText ctermbg=127 ctermfg=15

set t_te=
set t_ti=
