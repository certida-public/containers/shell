#!/bin/sh

if [ ! -f "/etc/phpmyadmin/config.secret.inc.php" ]; then
        cat > /etc/phpmyadmin/config.secret.inc.php <<__EOF__
<?php
\$cfg['blowfish_secret'] = '$(tr -dc 'a-zA-Z0-9~!@#$%^&*_()+}{?></";.,[]=-' < /dev/urandom | fold -w 32 | head -n 1)';
__EOF__
fi
mkdir -p /tmp/phpmyadmin
chmod 1777 /tmp/phpmyadmin
chmod o-w,a-x /etc/phpmyadmin/*.php

mkdir -p /opt/pgadmin4/var/config
mkdir -p /opt/pgadmin4/var/storage
mkdir -p /var/log/pgadmin
chmod 1777 /opt/pgadmin4/var/config
chmod 1777 /opt/pgadmin4/var/storage
chmod 1777 /var/log/pgadmin

NAMESERVERS=`grep '^nameserver' /etc/resolv.conf | awk '{print $2}' | xargs echo`
echo "resolver \"$NAMESERVERS\" valid=5s;" > /tmp/resolvers.conf;

if [ ! -z "$PMA_HOST" ]; then
  sed -i "s/@@MARIADB_PASSWORD@@/$PMA_PASSWORD/g" /var/www/html/index.html

  cat <<__EOF__ > /root/.my.cnf
[client]
user=$PMA_USER
password=$PMA_PASSWORD
host=$PMA_HOST
__EOF__

  chmod 0400 /root/.my.cnf
fi

if [ ! -z "$RABBITMQ_ERLANG_COOKIE" ]; then
  echo "$RABBITMQ_ERLANG_COOKIE" > /root/.erlang.cookie
  chmod 0600 /root/.erlang.cookie

  mkdir -p /var/lib/rabbitmq
  echo "$RABBITMQ_ERLANG_COOKIE" > /var/lib/rabbitmq/.erlang.cookie
  chmod 0400 /var/lib/rabbitmq/.erlang.cookie
  chown rabbitmq:rabbitmq /var/lib/rabbitmq/.erlang.cookie
fi

if [ ! -z "$RABBITMQ_PASSWORD" ]; then
  sed -i "s/@@RABBITMQ_PASSWORD@@/$RABBITMQ_PASSWORD/g" /var/www/html/index.html

  cat <<__EOF__ > /root/.rabbitmqadmin.conf
[default]
hostname = rabbitmq
username = root
password = $RABBITMQ_PASSWORD
__EOF__

  chmod 0400 /root/.rabbitmqadmin.conf
fi

if [ ! -z "${IJWT_AUTHORITY}" ]; then
  cat <<__EOF__ > /etc/nginx/conf.d/ijwt.inc
    location = /oidc/ijwt {
        proxy_set_header Authorization "Basic ${IJWT_CREDENTIALS}";
        proxy_set_header X-Forwarded-For "";
        proxy_set_header X-Forwarded-Proto \$http_x_forwarded_proto;

        proxy_pass http://${IJWT_AUTHORITY}/oidc/token;
    }
__EOF__
fi

if [ ! -z "$KUBERNETES_NAMESPACE" ]; then
  if [ $(echo "$KUBERNETES_NAMESPACE" | cut -c 1-12) = "review-epic-" ]; then
    cp -f /var/www/html/favicon-epic.ico /var/www/html/favicon.ico
  elif [ $(echo "$KUBERNETES_NAMESPACE" | cut -c 1-7) = "review-" ]; then
    cp -f /var/www/html/favicon-review.ico /var/www/html/favicon.ico
  elif [ "$KUBERNETES_NAMESPACE" = "staging-env" ]; then
    cp -f /var/www/html/favicon-staging.ico /var/www/html/favicon.ico
  elif [ "$KUBERNETES_NAMESPACE" = "production-env" ]; then
    cp -f /var/www/html/favicon-production.ico /var/www/html/favicon.ico
  else
    cp -f /var/www/html/favicon-testing.ico /var/www/html/favicon.ico
  fi

  if [ -d "/config-html" ]; then
    for f in $(ls /config-html); do
      cp -f "/config-html/$f" "/var/www/html"
    done
  fi

  if [ -f "/var/run/secrets/kubernetes.io/serviceaccount/token" -a ! -f "/root/.kube/config" ]; then
    KUBERNETES_CA_CRT=$(base64 /var/run/secrets/kubernetes.io/serviceaccount/ca.crt | xargs echo | sed 's/ //g')
    KUBERNETES_TOKEN=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)

    mkdir -p /root/.kube
    cat <<__EOF__ > /root/.kube/config
apiVersion: v1
kind: Config
clusters:
- name: default-cluster
  cluster:
    certificate-authority-data: "${KUBERNETES_CA_CRT}"
    server: "https://${KUBERNETES_SERVICE_HOST}"
contexts:
- name: default-context
  context:
    cluster: default-cluster
    namespace: "${KUBERNETES_NAMESPACE}"
    user: default-user
users:
- name: default-user
  user:
    token: "${KUBERNETES_TOKEN}"
current-context: default-context
__EOF__
  fi

  if [ "${KUBERNETES_NAMESPACE}" = "production-env" ]; then
    cat <<__EOF__ > /root/.bashrc_kube_ps1
kube_ps1() {
  echo -e " \\e[1;33m(\\u2388 ${KUBERNETES_NAMESPACE})\\e[0m"
}
__EOF__
  else
    cat <<__EOF__ > /root/.bashrc_kube_ps1
kube_ps1() {
  echo -e " \\e[1;34m(\\u2388 ${KUBERNETES_NAMESPACE})\\e[0m"
}
__EOF__
  fi
fi

if [ ! -z "$VOUCH_HOST" ]; then
  sed "s/@@VOUCH_HOST@@/$VOUCH_HOST/g" /etc/nginx/conf.d/auth-vouch.inc > /etc/nginx/conf.d/auth.inc
fi

if [ -d "/root/.dotfiles" ]; then
  for f in $(ls -A /root/.dotfiles); do
    if [ "$f" != "README.md" ]; then
      rm -f "/root/$f"
      ln -s ".dotfiles/$f" "/root/$f"
    fi
  done
fi

env | sort > /root/.shell-env

exec /usr/bin/supervisord -c /etc/supervisord.conf
