#!/bin/sh

if [ -z "${REDIS_1_HOST}" ]; then
  while true; do
    sleep 86400
  done
  exit 0
fi

/usr/local/bin/wait-for.sh -t 0 "${REDIS_1_HOST}:${REDIS_1_PORT}"

if [ -z "${REDIS_1_AUTH}" ]; then
  exec /usr/local/bin/redis-commander -p 8666 \
    --redis-host "${REDIS_1_HOST}" \
    --redis-port "${REDIS_1_PORT}" \
    --trust-proxy \
    --url-prefix "/redis"
fi

exec /usr/local/bin/redis-commander -p 8666 \
  --redis-host "${REDIS_1_HOST}" \
  --redis-port "${REDIS_1_PORT}" \
  --redis-password "${REDIS_1_AUTH}" \
  --trust-proxy \
  --url-prefix "/redis"
