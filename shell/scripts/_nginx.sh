#!/bin/sh

nginx -t || exit 1

while true; do
  nginx -g 'daemon off;'
  sleep 1
done
