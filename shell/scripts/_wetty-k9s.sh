#!/bin/sh

if [ ! -f "$HOME/.kube/config" ]; then
  echo "You must have a Kubernetes configuration in '$HOME/.kube/config' to use this."
  read
  exit 1
fi

exec /usr/local/bin/k9s
