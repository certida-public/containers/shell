#!/bin/sh
#
# From: https://serverfault.com/questions/760726/how-to-exit-all-supervisor-processes-if-one-exited-with-0-result

echo "READY"

while read line; do
    echo "$line" >&2
    kill -SIGQUIT "$PPID"
done < /dev/stdin
