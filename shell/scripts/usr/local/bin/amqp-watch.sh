#!/bin/sh

if [ -f "$HOME/.rabbitmqadmin.conf" ]; then
  AMQP_HOSTNAME=$(grep '^hostname =' "$HOME/.rabbitmqadmin.conf" | awk '{print $3}')
  AMQP_USERNAME=$(grep '^username =' "$HOME/.rabbitmqadmin.conf" | awk '{print $3}')
  AMQP_PASSWORD=$(grep '^password =' "$HOME/.rabbitmqadmin.conf" | awk '{print $3}')
else
  AMQP_HOSTNAME="rabbitmq"
  AMQP_USERNAME="user"
  AMQP_PASSWORD="Zxcvbn456"
fi

AMQP_EXCHANGE="$1"
if [ -z "${AMQP_EXCHANGE}" ]; then
  AMQP_EXCHANGE="Ph.Platform.Events.IntegrationEvents:IntegrationEvent"
fi

exec amqp-consume -u "amqp://${AMQP_USERNAME}:${AMQP_PASSWORD}@${AMQP_HOSTNAME}:5672" --exchange="${AMQP_EXCHANGE}" --routing-key='#' jq
