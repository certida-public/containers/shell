location /csstidy {
    root /opt;
    index  index.php index.html;

    try_files $uri $uri/ @csstidy;

    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass 127.0.0.1:9000;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param SCRIPT_NAME $fastcgi_script_name;
        fastcgi_index index.php;
        include fastcgi_params;
    }

    location ~ /\. {
        log_not_found off;
        deny all;
    }
}

location @csstidy {
    rewrite /csstidy/(.*)$ /csstidy/index.php?/$1 last;
}
