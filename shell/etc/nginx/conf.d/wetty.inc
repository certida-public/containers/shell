location = /wetty-k9s/favicon.ico {
  rewrite ^.*$ /favicon.ico;
}

location ^~ /wetty-k9s {
  proxy_pass http://127.0.0.1:8089/wetty-k9s;
  proxy_http_version 1.1;
  proxy_read_timeout 43200000;

  proxy_set_header Upgrade $http_upgrade;
  proxy_set_header Connection "upgrade";
  proxy_set_header Host $http_host;
  proxy_set_header X-Real-IP $remote_addr;
  proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
  proxy_set_header X-NginX-Proxy true;
}

location = /wetty-logs/favicon.ico {
  rewrite ^.*$ /favicon.ico;
}

location ^~ /wetty-logs {
  proxy_pass http://127.0.0.1:8086/wetty-logs;
  proxy_http_version 1.1;
  proxy_read_timeout 43200000;

  proxy_set_header Upgrade $http_upgrade;
  proxy_set_header Connection "upgrade";
  proxy_set_header Host $http_host;
  proxy_set_header X-Real-IP $remote_addr;
  proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
  proxy_set_header X-NginX-Proxy true;
}

location = /wetty-shell/favicon.ico {
  rewrite ^.*$ /favicon.ico;
}

location ^~ /wetty-shell {
  proxy_pass http://127.0.0.1:8088/wetty-shell;
  proxy_http_version 1.1;
  proxy_read_timeout 43200000;

  proxy_set_header Upgrade $http_upgrade;
  proxy_set_header Connection "upgrade";
  proxy_set_header Host $http_host;
  proxy_set_header X-Real-IP $remote_addr;
  proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
  proxy_set_header X-NginX-Proxy true;
}
