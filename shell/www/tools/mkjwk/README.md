# mkjwk

This was built from the code from <https://github.com/mitreid-connect/mkjwk.org>.

To re-create `bundle.js`:

1. Replace the files in `src/main/js/` with the ones in `src/`.
2. Run `npm install`.
3. Run `npm install node-jose`.
4. Run `npm run-script watch`. After webpack builds, extract `bundle.js` from its built location (found in the webpack output).
