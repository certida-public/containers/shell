var deployed = window.location.hostname.indexOf('.') > 0 &&
               !(window.location.hostname.endsWith('.host')        ||
                 window.location.hostname.endsWith('.local')       ||
                 window.location.hostname.endsWith('.localdomain') ||
                 window.location.hostname.endsWith('.localhost')   ||
                 window.location.hostname.endsWith('.mshome.net'));
if (deployed) {
  body.setAttribute('data-deployed', '1');
}

var replaceDeployed = {
};

var links = document.getElementsByTagName('a');
for (var i = 0; i < links.length; i++) {
  var href = links[i].getAttribute('href');
  if (href.startsWith(':')) {
    if (deployed && replaceDeployed[href]) {
      var replace = replaceDeployed[href];
      if (replace.endsWith('-')) {
        links[i].href = window.location.protocol + '//' + replace + window.location.hostname;
      } else {
        links[i].href = replace;
      }
    } else {
      links[i].href = window.location.protocol + '//' + window.location.hostname + href;
    }
  }
}
